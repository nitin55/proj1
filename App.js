import React, { Component } from 'react';
import './App.css';
// import Select, { components } from 'react-select';
// import MySelect from './Component/MySelect';
// import { Jumbotron, Container } from 'react-bootstrap';
// import SS from './Component/DateField/sss';
// // import Input from './Component/Input/input';
// import TextInput from './Component/MaterialText/textinput';
import Navbar from './Component/Navbar/Navbar';
// import DataTable from './Component/Table/Table'
import Main2 from './Container/main2'
import Main3 from './Container/Main3'
import { Route, Switch, Link, Redirect } from 'react-router-dom';
// import Main from './Container/Main';
import Screen2 from './Container/screen2';
import Login1 from './Component/Login/Login1';
import AppFrame from './Container/appFrame/appFrame';
// import ActivityPlan1 from './Container/ActivityPlanning/ActivityPlan1';
// import ActivityPlan2 from './Container/ActivityPlanning/activityplanning2';
// import ActivityPlan3 from './Container/ActivityPlanning/Activityplan3/activityplan3';
import Table2 from './Component/Table/Table2';
// import Sidebar from './Component/Sidebar/Sidebar'
import Dashboard from './Container/Dashboard/Dashboard';
import Dashboard2 from './Container/Dashboard/dashboard2';
import Dashscreen2 from './Component/dashboardscreens/screen2/screen2';
import Dashscreen3 from './Component/dashboardscreens/screen3/screen3';
import TaskScreen from './Component/dashboardscreens/tasksscreen/tasksscreen'
import Abc from './Component/Tree/abc/abc'
import Trees2 from './Component/Tree/Tree2';
import Trees1 from './Component/Tree/Tree';
import AdminDash from './Container/Dashboard/AdminDash';
import ClientDash from './Container/Dashboard/ClientDash';



class App extends Component {
  render() {
    return (
      <div>
        {/* <Navbar path={window.location.href}/> */}
        <Switch>
          <Route path="/" exact component={Login1} />
          {/* <Route path="/Dashboard" exact component={Dashboard} /> */}
          {/* <Route path="/table" component={AppFrame} /> */}
          <Route path="/activity" component={Main3} />
          <Route path="/activity2" component={Screen2} />
          <Route path="/table2" component={Table2} />
          <Route path="/Dashboard" component={Dashboard2} />
          <Route path="/screen2" component={Dashscreen2} />
          <Route path="/screen3" component={Main3} />
          <Route path="/taskscreen" component={TaskScreen} />
          <Route path="/abc" component={Trees2} />
          {/* <Route path="/abc1" component={Trees1} /> */}
          <Route path="/AdminDash" component={AdminDash} />
          <Route path="/ClientDash" component={ClientDash} />
          {/* <Route path="/Login" exact component={Login1} /> */}
          <Route path="/404" render={function () {
            return <center><h4 style={{ marginTop: "10%" }}>Not found</h4></center>
          }} />
          <Redirect from="*" to="/404" />
        </Switch>
        {/* {console.log(window.location.origin)} */}
      </div>
    )
  }


}

export default App;





{/* <Input type="text" noOptionsMessage={() => null} className="inputOnly react-select-container"
     
        classNamePrefix="react-select"
        onChange={this.handleChange}
      /> */}

{/* <Select
        noOptionsMessage={() => null}
        value={selectedOption1}
        options={options}
        onChange={this.handleChange1}
        isMulti={false}
        isSearchable={false}
        placeholder="Select single option"
      /> */}


{/* <Input
      label="Start Date"
      type="date"
      placeholder="Start Date"
      />

      <Input 
       label="End Date"
       type="Date"
       placeholder="End Date"
      /> */}


{/* <Input 
      label="Client File Number"
      type="text"
      placeholder="Client File Number"
      />

      <Input 
      label="Client Name"
      type="text"
      placeholder="Client Name"
      /> */}